# README

Scripts accompagnés de la documentation utilisateur permettant de realiser les analyses à partir de la **matrice de comptage _individus-variables_** .

## Script de traitements.

Ce script va charger préalablement le fichier *count_matrix.tsv* présent dans le dossier `data.preparation/` de ce repos Git. Il est donc nécessaire d'avoir executer le script `data.preparation/projet_fouille_pretraitement.R` pour pouvoir faire tourner ce script.

Pour le lancer il necessite les librairies suivantes :

- Pour la manipulation des variables : `tidyverse , magrittr`
- Pour la visualisation des figures : `ggplot2 , cowplot`
- Pour l'analyse des données (ACP, Clustering) : `FactoMineR , cluster`
- Pour la manipulation de dendogramme : `dendextend`

Le bloc intitulé **"[Installation et chargement des librairies](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L8)"** permet de vérifier la présence de ces librairies sur l'ordinateur avant de les installer, si besoin, et de les charger.

## Plan du script

Il se décompose en 9 partie :

1) [Chargement des données et calcul de la matrice de distance.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L17)
2) [Creation d'un dossier pour collecter les figures.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L32)
3) [Projection dans un plan des données via ACP et MDS.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L42)
4) [Representation graphique de la matrice de comptage.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L59)
5) [Implementation du clustering Kmeans.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L70)
6) [Implementation du clustering Kmedoids.](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L100)
7) **WIP** - Coefficient de silhouette.
8) [Implementation du clustering hiérarchique par liens complet](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L132)
9) [Comparaison graphiques des différentes méthodes](https://gitlab.com/m1-bbs/m1-bbs-projet-fouille-boon-martin/-/blob/master/analysis/projet_fouille_analyses.R#L184)

Les résultats de ces traitements ont été traité dans le rapport au format *MARKDOWN* présent dans le répértoire `rapport/` du repos Git.
