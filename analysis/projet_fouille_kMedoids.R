######################################################################
###################### PROJET FOUILLE - Analyse ######################

# Sous RStudio, préparation de l'environnement de travail :
## Si ce script est lancé sous RStudio, décomenter la ligne suivante :
## setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# Chargement des librairies :
library(tidyverse)
library(ggplot2)
library(magrittr)

# Chargement de la matrice individus-variables :
count_matrix <- read.table("../data.preparation/count_matrix.tsv", header=T, sep="\t")

# Realisation de la matrice de distance :
T0 <- Sys.time()
dist_matrix <- dist(count_matrix, diag=T, upper=T)  
dist_matrix <- as.matrix(dist_matrix)
T0 - Sys.time() ### Pour info : Time difference of -0.006719112 secs

# Ecriture de la matrice dans un fichier :
write.table(dist_matrix, file="dist_matrix.tsv", row.names=T, col.names=T, sep="\t")

# Projection 2D par multidimentional scalling :
mds = cmdscale(dist_matrix)

# Sauvegarde des coordonées :
count_matrix %<>% mutate(x = mds[,1], y = mds[,2])

# Test K-means
## Determination du meilleur K
ratio_ss <- data.frame(cluster = seq(from = 5, to = 50, by = 5)) 
for (k in ratio_ss$cluster) {
  km_model <- kmeans(count_matrix[,1:194], k, nstart = 40)
  ratio_ss$ratio[which(ratio_ss$cluster==k)] <- km_model$tot.withinss / km_model$totss
}

## Representation graphique
ggplot(ratio_ss, aes(cluster, ratio)) + 
  geom_line() +
  geom_point() +
  geom_vline(xintercept = 15, col="red", linetype="dashed", size=0.75)
## Sauvegarde du resultat pour K=15
km_model <- kmeans(count_matrix, 15, nstart = 40)
count_matrix[,"cluster"] = as.factor(km_model$cluster)
## Representation graphique
count_matrix %>%
  ggplot(aes(x=x, y=y, color=cluster)) +
  geom_point(size = 3, alpha = 1)

# ---------- added medoids ----------

library(cluster)

pam_model = pam(count_matrix, k = 15, metric = "euclidean", stand = FALSE)
count_matrix[,"medoids"] = as.factor(pam_model$clustering)
## Representation graphique
count_matrix %>%
  ggplot(aes(x=x, y=y, color=medoids)) +
  geom_point(size = 3, alpha = 1)

## coef de silhouette
count_matrix %<>% mutate(x=mds[,1], y =mds[,2])
d = as.matrix(dist_matrix)
tapply(d[1,], pam_model$clustering, mean) # calcul de distance pour la 1ere ligne
tapply(d[1,-1], pam_model$clustering[-1], mean) # calcul de distance pour la 2eme ligne



