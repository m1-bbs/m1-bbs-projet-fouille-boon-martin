[[_TOC_]]

# README

Scripts accompagnés de la documentation utilisateur permettant d'obtenir la matrice *individus-variables* qui servira pour le projet.

## Script de pré-traitements.

Ce script permet d'enregistrer le fichier *count_matrix.tsv* qui correspond à la **matrice de comptage _individus-variables_**.

Pour le lancer il ne nécessite pas l'utilisation de librairies autre que les packages compris dans R. Tout la construction est issus de la table *Assembly.tsv* qui doit être présent accessible à partir du chemin `../data/Assembly.tsv`.

## La matrice :

La matrice individus-variables est une matrice de 110 x 194.

Les inidividus sont représenté par les différentes *Functionnal_Classification* enregistré dans la table et les variables sont les différentes *Domain_Organization* et elle a été initialisé à 0.

Pour la remplir, nous avons pris chaque lignes de la table Assembly et pour chaque *Functionnal_Classification*, nous avons ajouter +1 à la colonne correspondantes au *Domain_Organization* de la ligne.

#### NOTES : Verification de la matrice

A partir de la ligne 37 du script R, il y a quelques instructions permettant de contrôler la construction de la matrice. En effet, la matrice doit renvoyer les mêmes résultats que la commande SQL renseignée dans le codes.

Pour ce qui est du count_vector, il permet de verifier que TOUT les Domain_Organization de la table Assembly ont bien été pris en compte.

## Utilisation de la matrice :

Cette matrice va servir de base pour réaliser différents clustering et permettre de comparer les différentes méthodes de clustering sur ce jeu de données expertisé