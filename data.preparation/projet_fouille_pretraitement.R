#######################################################################
################### PROJET FOUILLE - Pre-traitement ###################

# Sous RStudio, préparation de l'environnement de travail :
## Si ce script est lancé sous RStudio, décommenter la ligne suivante :
## setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

# Lecture du fichier tsv :
df <- read.table("../data/Assembly.tsv", header=T, sep="\t")
df <- df[,c("Functional_Classification","Domain_Organization")]

# Sauvegarde du nom des lignes et colonnes :
functional_classification <- sort(unique(df$Functional_Classification))
domain_organization <- sort(unique(toupper(df$Domain_Organization)))

# Creation et initialisation de la matrice de comptage à 0:
count_matrix <- data.frame(row.names = functional_classification)
for (domain in domain_organization) {
  count_matrix[,domain] = rep(0, length(functional_classification))
}

# Remplissage de la matrice de comptage :
T0 = Sys.time()
for (l in 1:nrow(df)) {
  count_matrix[which(rownames(count_matrix) == df[l,1]),which(domain_organization%in%df[l,2])] = count_matrix[which(rownames(count_matrix) == df[l,1]),which(domain_organization%in%df[l,2])]+1
}

# Ecriture de la matrice individu-variable dans un fichier :
write.table(count_matrix, file="count_matrix.tsv", row.names=T, col.names=T, sep="\t")

# Estimation temps de calcul :
Sys.time() - T0 ### Pour info : Time difference of 2.070817 secs

### VERIFICATION DE LA MATRICE ###

## La matrice de comptage peut être vérifiée à l'aide de la requête SQL suivante :
##
## SELECT Functional_Classification, Domain_Organization, count(Domain_Organization)
## FROM Assembly
## GROUP BY Functional_Classification, Domain_Organization
## ORDER BY Functional_Classification, Domain_Organization ASC;

# Vecteur permettant de compter le nombre d'occurences de chaques domaines.
count_vector = c()
for (i in 1:ncol(count_matrix)) {
  n = names(count_matrix)[i]
  v = sum(count_matrix[,i])
  count_vector[i] = v
  names(count_vector)[i] = n
}

## sort(count_vector) renvoie bien la même chose que la requete :
##
## SELECT Domain_Organization, count(Domain_Organization) FROM Assembly
## GROUP BY Domain_Organization
## ORDER BY Domain_Organization DESC;