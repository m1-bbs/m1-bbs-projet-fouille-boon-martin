**Répertoire à laisser vide sur gitlab** mais qui localement devrait contenir les fichiers CSV de départ :

- Assembly.tsv.gz
- Chromosome.tsv.gz
- Conserved_Domain.tsv.gz
- Functional_Domain.tsv.gz
- Gene.tsv.gz
- Orthology.tsv.gz
- Protein.tsv.gz
- Strain.tsv.gz
- Taxonomy.tsv.gz

**EDIT : 2020-05-11 10:10**
Dans le cadre de ce projet, nous n'avons besoin que de la table *Assembly.tsv*
