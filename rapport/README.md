# M1-BBS : Projet de Fouille de Donnée - 2019/2020
_**Quentin BOON, Jérémy MARTIN**_

_**Mardi 9 Juin, 2020 01:10**_

---

[[_TOC_]]

# Contexte

Le but de ce projet est de comparer trois méthodes de clustering sur des données expertisés des systèmes ABC.

## Les systèmes ABC

Les systèmes ABC forment une très grande **famille multigénique**. Ils sont présents dans les 3 règnes du vivant *(bactéries, archées et eucaryotes)*.

La plupart de ces systèmes sont des transporteurs dont **l’architecture** est conservée et est composée de 2 domaines pour les exporteurs et 3 domaines pour les importeurs.

Ces transporteurs permettent l'import/export de nombreux susbstrat et tire leurs energie de l'hydrolyse de l'ATP.

![Transporteur ABC](transporteurs.png "Transporteur ABC")

# Analyse

**Précisions sur les objectifs à atteindre et comment y arriver.** 

Pour pouvoir comparer différentes méthodes de clustering, il va être nécessaire d'extraire les données d'interêt à partir de la table **Assembly** qui contient les informations expertisés.

Nous avons fait le choix de nous concentrer sur les *Functional_Classification* et les *Domain_Organization* pour étudier s'il était possible de regrouper les nombreuses classifications en cluster. 

Les méthodes de clustering choisis sont :
- deux clustering par partitionnement : **K-means** et **K-médoïdes**.
- un **clustering hierarchique**, ici avec *liens complet*.

Le choix de ces méthodes est entièrement arbitraire.

**Analyse préliminaire des données pour les appréhender ainsi que les méthodes disponibles pour atteindre les objectifs.**

La table **Assembly** est constitué de 6002 observations et de 5 colonnes.

Cette table détient les informations suivantes :

![Assembly table](00_assembly_table.png "Assembly")

Nous ne nous concentrerons que sur deux d'entre elles : *Functional_Classification* et *Domain_Organization* afin de tester s'il est possible ou non de regrouper en cluster la classification fonctionnelle des systèmes ABC en fonction de l'organisation de leurs domaines protéiques.

Il a donc fallut se poser les questions suivantes :

1) Comment obtenir une matrice individus-variables à partir des 6002 observations ?
2) Comment visualiser les données de la matrice individus-variables ?
3) Comment déterminer le nombres de cluster adequat pour la réalisation du clustering par partitionnement ?
4) Comment visualiser graphiquement l'ensemble de ces résultats ?
5) Comment évaluer la qualités de ces différentes méthodes ?

# Conception

**Fort de l'analyse précedente, présenter l'approche choisie et pourquoi. Quelle méthodologie allez-vous mettre en oeuvre pour les différentes étapes du projet avec quelles méthodes (par exemple : obtention de la matrice pour l'analyse, puis quelle méthode de classification et comment se fera l'évaluation).**

## La matrice individus-variables.

Dans notre étude, les individus sont les différentes *Functional_Classification* enregistré dans la table et les variables sont les différentes *Domain_Organization*.

Pour la remplir, nous avons pris chaque lignes de la table Assembly et pour chaque *Functional_Classification*, nous avons ajouter +1 à la colonne correspondantes au *Domain_Organization* de la ligne.

Pour créer cette matrice, il a fallut dans un premier temps "sauvegarder" le nom des individus et des variables :

```R
functional_classification <- sort(unique(df$Functional_Classification))
domain_organization <- sort(unique(toupper(df$Domain_Organization)))
```

Puis initialiser la création de la matrice à 0 :

```R
count_matrix <- data.frame(row.names = fuctional_classification)
for (domain in domain_organization) {
    count_matrix[,domain] = rep(0, length(functional_classification))
}
```
Voilà la matrice créer.
Cette dernière se présente donc de la forme :

|     \     | AUX-MSD,OTHER-MSD,NBD,SBP | FHA-FHA-NBD-MSD | MSD | MSD-MSD | ... |
|:---------:|:-------------------------:|:---------------:|:---:|:-------:|:---:|
|  **A_?**  |             0             |        0        |  0  |    0    | ... |
|  **A_0**  |             0             |        0        |  0  |    0    | ... |
|  **A_1**  |             0             |        0        |  0  |    0    | ... |
|  **A_10** |             0             |        0        |  0  |    0    | ... |
| **A_10a** |             0             |        0        |  0  |    0    | ... |
|  **...**  |            ...            |       ...       | ... |   ...   | ... |

Une fois la matrice accessible, il ne reste plus qu'à la remplir. Pour cela, reprenons le tableau **df** chargé en entrée. Ce dernier se présente comme ceci :

 Functional_Classification | Domain_Organization |
|---------------------------|--------------------:|
| A_8a                      |         MSD,NBD,SBP |
| A_7a                      |             MSD,NBD |
| A_7a                      |             MSD,NBD |
| A_10a                     |         MSD,NBD,NBD |
| ...                       |                 ... |

Pour remplir la matrice de comptage, il faut donc suivre l'algorithme suivant :

```
POUR chaque ligne L de df :
    df[L, Functional_Classification] = individu
    df[L, Domain_Organization] = variable
    POUR chaque ligne R de de count_matrix :
        Si rownames(count_matrix)[R] == individu, alors :
            count_matrix[R, variable] += 1
```
Ainsi, pour chaque Functional_Classification, notre matrice de comptage ajoutera 1 à la colonne correspondant au Domain_Organization !

Cependant, R ne gère pas très bien les double boucles (temps de calcul extremement long). Cette double boucle à pu être simplifier de la façon suivante :
```R
for (l in 1:nrow(df)) {
  count_matrix[which(rownames(count_matrix) == df[l,1]),which(domain_organization%in%df[l,2])] = \
    count_matrix[which(rownames(count_matrix) == df[l,1]),which(domain_organization%in%df[l,2])]+1
}
```

## Comment visualiser les données de la matrice individus-variables ?

Afin de pouvoir "visualiser" les données, il est nécessaire dans un premier temps de les projeter dans un plan. Pour cela, il a été réalisé deux approches à partir de la **matrice de distance** issue de nos données :
- Une approche par **Analyse des Composantes Principales** (ACP)
- Une approche par **Multidimensional Scalling** (MDS)

![Projection des données](.fig/00_matrix_representation.png "Projection des données")

Ces deux approches nous permettent d'observer selon deux angles de vue différents, les mêmes données. Ces deux projections ont été utilisé tout au long de l'analyse afin de voir si visuellement, elle pouvait jouer un rôle dans l'interpretation que l'on pouvait avoir à l'issue des différents clustering.

## Demarche préliminaire au clustering par partitionnement :

Dans le cadre du clustering par partitionnement, qu'il s'agisse d'une méthode K-Means ou K-médoides, il est nécessaire de choisir à l'avance le nombre de clusters en sortie.

Pour ce faire, nous avons évalués la somme des écarts aux carrées des erreurs pour différentes valeurs de K.
Quand le SSE scesse de chuter drastiquement, l'ajout de nouveau cluster ne permet pas d'améliorer le clustering.

![Elbow graph](.fig/01_best_K.png "Elbow graph")

Dans notre cas, il semblerait qu'une valeur de K = 30 permette de réaliser un clustering correct.

## La visualisation des résultats :

La visualisation des clustering se fera en assignant une couleur à chacun des clusters et en ajoutant cette information dans le tableau "count_matrix".

De cette manière, il sera possible de visualiser les différents clusters établis directement sur la projection des données dans le plan (que cette projection soit faite par ACP ou MDS).

Dans un soucis de lisibilité, les librairies **ggplot2**, **cowplot** et **dendextend** seront utilisés pour visualiser les résultats.

## L'évaluation de la qualité du clustering

Pour comparer toutes ces méthodes, une méthode d'évaluation de la qualité du clustering non supervisé semble la plus adéquate.

Nous avons essayé de mettre en place un calcul de coefficiant de silhouette pour essayé de visualiser l'homogénéité au sein des différents clusters, mais n'avons pour le moment pas reussi à l'implementer correctement.

# Réalisation

**Mise en oeuvre de ce qui a été conçu. Il s'agit la de préciser les paramètres et tous les détails concernant la réalisation concrète de l'analyse. Puis de décrire les résultats obtenus.**

## K-means :

La première méthode de clustering implémentée fût celle de K-means. Elle est nativement prise en charge par R et ne nécessite que la matrice de comptage et la valeur de K évalué précédemment :

```R
km_model <- kmeans(count_matrix, k = 30, nstart = 40)
```

La représentation de ce clustering est la suivante :

![Clustering Kmeans](.fig/02_kmeans_representation.png "Kmeans Representation")

On observe une mauvaise séparation de nombreux cluster proche du point [-5,0]. 

## K-medoides :

La seconde méthode de clustering implémentée fût celle de K-medoides. Il a été choisi d'utiliser la fonction pam de la librairie "cluster".

```R
pam_model <- pam(count_matrix, k = 30, metric = "euclidean", stand = FALSE) # Les données ne sont pas normalisés.
```

La représentation de ce clustering est la suivante :

![Clustering Kmedoides](.fig/02_kmedoids_representation.png "Kmedoides Representation")

Ici aussi, on retrouve le même problème qu'avec le clustering Kmeans peut importe la projection utilisée.

## Clustering hiérarchique à liens complet :

Encore une fois, c'est la librairie "cluster" qui a été utilisé pour sa fonction hclust :
Le clustering hiérarchique ne nécessite pas de préciser le nombre de cluster, il s'agit d'un clustering dit *non-supervisé*, mais prend en entrée une matrice de distance.

```R
dist_matrix <- count_matrix %>% 
                        daisy(metric = "euclidean")
                        
h1 <- hclust(dist_matrix, method = "complete")
```

La fonction `cutree()` permet de "découper" le dendogramme en K clusters. Pour pouvoir comparer les différentes méthodes utilisées jusqu'ici, il a été choisi de rester sur un K = 30 :

```R
hclust <- as.factor( cutree(h1, k = 30) )
```

Le résultats de ce clustering hiérarchique peut se présenter sous deux formes :

- Dendograme : 

![Hclust Dendograme](.fig/03_hclust_dendogramme.png "Dendogramme clustering hiérarchique")

- Projection dans le plan : 

![Hclust Projection](.fig/03_hclust_proj.png "Projection dans le plan clustering hiérarchique")

On observe alors graphiquement, une bien meilleure séparation des clusters que précédemment, bien qu'imparfaite.

# Discussion

**Analyse et discussion sur les résultats obtenus.**

**Conclusions sur la qualité de la ou des méhtodes mises en oeuvre.**

On observe une faible différence dans la structure des clusters entre les deux méthodes par partitionnement, mais ces deux méthodes différent grandement du clustering hiérarchique.

Sur la figure suivante, on observe sur les branches du dendograme, le clustering hiérarchique, et au niveau des feuilles, les couleurs correspondent au clustering par partitionnement :

![Comparaison Part. Vs HClust](.fig/04_hclust_dendo_+_clusters.png "HClust Vs Partition")

On remarque alors, que pour un même groupe de "branches", on a differentes couleurs de "feuille". Le clustering hierarchique ayant été conçu directement à partir de la matrice de distance, cela signifie que les méthodes de clustering par partitionnement regroupe dans un même cluster, des individus éloignés.

A quelques différences près les méthodes k-means et k-medoïdes sont très semblables mais on remarque tout de même que la méthode k-means et celle des deux qui se rapproche le plus du clustering hierarchique.

Voici une synthèse graphique de nos résultats dans les deux projections réalisés au début du projet :

- Projection MDS :

![Synthèse MDS](.fig/05_MDS_projection.png "Bilan MDS")

Sur ces projections les résultats ne sont pas bien mis en évidence puisque tout les individus sont concentrés de façon orthogonale dans la plan. On constate néanmoins que les clustering k-means et k-medoïdes présentes des clusters quasiment identiques alors que le clustering hiérarchique ne fait pas tout a fait les même regroupements. 

Si on se focalise sur les clusters qui apparaissent comme étant proche de l'origine, on remarque un cluster avec la méthode hiérarchique qui est divisé en 2 clusters pour les autres. On peut supposer qu'il y a de faibles distances entre les individus mais qu'ils sont suffisamment éloignés pour être découpés en deux groupes en fonction de la méthode employée.

- Projection ACP :

![Synthèse ACP](.fig/05_PCA_projection.png "Bilan ACP")

Avec la projection par ACP les individus sont plus dispersé qu'avec la projection MDS permettant de mieux visualiser les différents clusters formés. On retrouve cependant les même informations que précédemment.

Il est possible d'observer sur le dendrogramme que le regroupement par méthode hierarchique donne lieu à une séparation en deux sous arbres : une partie à droite relativement homogène, où toutes les branches d'un même cluster descendent d'un noeud commun, et une seconde partie à gauche très hétérogènes avec des clusters de petites taille. Cela peut expliquer les différences relevés avec les méthodes de clustering par partitionnement

On sait que les individus sont représenté par les *Functional_Classification* et les variables sont les *Domain_Organization*, les clusters vont donc être construit a partir des distances entre leur ogranisation en domaines et regrouper plusieurs classes fonctionnelles. 

Les différences entre les méthodes vont donc faire varier l'appartenance d'une classe fonctionnelle à un regroupement de domaines ce qui peut donner lieu à des incohérences si le cluster n'est pas bien représentatif. Dans tout les cas, et quel que soit la méthode employée, on remarque la présence d'un gros cluster qui regroupe une large partie des données puis un ensemble de petit cluster (des fois ne contenant qu'un seul indivuds) autours de ce dernier. On peut donc supposer qu'il y a un grand nombre d'individus (Functional_Classification) qui ont des organisation similaires ou relativement proche. Les autres individus auraient des organisations en domaines plus atypique, qui ne présenteraient pas ou peu de similarité entre eux. 

- nombre d'individus par cluster avec la méthode k-means :
```
     [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8] [,9] [,10] [,11] [,12] [,13] [,14] [,15] [,16] [,17]
[1,]    3    4    5    3    4    1    5    6    7     2     1     1     1     4     1     1     1
     [,18] [,19] [,20] [,21] [,22] [,23] [,24] [,25] [,26] [,27] [,28] [,29] [,30]
[1,]    32     6     2     1     1     1     3     4     2     1     1     1     5
```

- nombre d'individus par cluster avec la méthode k-medoïdes :
```
     [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8] [,9] [,10] [,11] [,12] [,13] [,14] [,15] [,16] [,17]
[1,]   35    8    1    2    1    1    4    4    1     6     8     6     1     5     1     4     1
     [,18] [,19] [,20] [,21] [,22] [,23] [,24] [,25] [,26] [,27] [,28] [,29] [,30]
[1,]     5     1     1     1     1     1     5     1     1     1     1     1     1
```

- nombre d'individus par cluster avec la méthode hiérarchique :
```
     [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8] [,9] [,10] [,11] [,12] [,13] [,14] [,15] [,16] [,17]
[1,]   56    4    1    2    1    1    1    2    1     3     7     1     1     5     1     9     1
     [,18] [,19] [,20] [,21] [,22] [,23] [,24] [,25] [,26] [,27] [,28] [,29] [,30]
[1,]     1     1     1     1     1     1     1     1     1     1     1     1     1
```

On notera que le numero qui correspond au cluster dans chaque cas n'est pas significatif, le numero de cluster est attribué par l'algorithme utilisé donc un cluster qui regroupe les même individus pour 2 méthodes différentes peuvent ne pas avoir la même numérotation.

Au final il semblerait que le regroupement des individus *Functional_Classification* en cluster basé sur les distances des *Domain_Organization* ne permet pas d'identifier correctement les groupes d'individus. En effet, quel que soit la méthode utilisée on se retrouve avec un groupe majoritaire qui englobe une grosse proportion des individus et la plupart des autres groupes contiennent trop peu de membres pour etre significatif.

# Bilan et perspectives

**Qu'est-ce qui fonctionne ou pas. Piste d'amélioration. Recul sur l'ensemble du projet. Si c'était à refaire...**

Au niveau du travail réalisé, l'extraction des données et le traitement pour réaliser les matrices de comptage et de distances ont été correctement menées. Même constat en ce qui concerne l'utilisation de ces dernières pour les 3 méthodes de clustering étudiées, à savoir k-means, k-medoïdes et hiérarchique. En revanche nous ne somme pas parvenus à faire fonctionner le coefficient de silhouette qui nous aurait permis de mesurer la qualité de façon non supervisé de notre analyse. 

Une première piste d'amélioration serait l'implémentation d'une ou plusieurs méthodes d'analyse de la qualité des méthodes implémenter. Il serait également intéressant de pousser la comparaison du clustering hiérarchique via d'autre calcul de distance (Ward, $`\chi^2`$ , ...). Au vu de la taille réduite des données d'études, il n'a pas été nécessaire de développer l'aspect "complexité" de notre analyse. Il serait cependant pertinent de mettre en place une mesure de cette complexité si ces méthodes venait à être utilisé sur de plus grand jeux de données. Enfin, une dernière piste d'amélioration serait de tester les scripts sur des jeux de données différents à conditions d'obtenir en entrée une matrice de contingence.

Dans l'ensemble le projet s'est correctement déroulé malgrès le fait que les échanges ont été rendus compliqué par la periode de confinement. Cela a inéluctablement impacté l'avancement du projet. Le travail demandé semblait tout de même accessible, en grande partie grâce a l'encadrement maintenue et soutenue au regards des complications mentionnées ci-dessus.

# Gestion du projet

**Comment s’est organisé le groupe. Comment se sont déroulées les discussions, les prises de décisions. Comment se sont répartities les tâches. Quels ont été les rôles et les contributions de chacun·e. Diagramme de Gantt avec le calendrier et les tâches.**

L'ensemble des prises de décisions ont été fait durant des réunions téléphoniques au cours du confinement. Il a ainsi pu être établis les grands axes du développement de notre projet et la répartition des taches s'est réalisée petit à petit au cours de l'avancement du projet.

Au final, il a été possible de mettre en place le diagramme de Gantt suivant :

![GanttProject](Gantt.png "Diagramme de Gantt")

- Les tâches en bleu correspondent aux étapes de réflexions et de mise en place du projet. Elles ont toutes été réalisé entierrement en duo durant des séances de "télé-réunions".
- Les tâches en verte correspondent aux étapes d'implémentation des algorithmes. La première implémentation a été réalisé en groupe afin de se (re-)familiariser avec les mécanismes propre de R. Par la suite, nous avons pris la décision de travailler individuellement sur chacune des deux autres méthodes et de réaliser une mise en commun ultérieurement.
- Les tâches en orange correspondent aux étapes d'implémentation et de gestion de GitLab. Jérémy ayant eu l'occasion de se familiariser avec Git dans le cadre de la préparation d'un stage s'est occupé de cette partie.
- Les tâches en rouge correspondent aux étapes de rédactions et de documentations. Ces étapes ont été réalisé par les deux parties avant une mise en commun pour fournir le présent document, mais aussi l'ensemble des READ ME accessible dans les sous dossier du repos.

Comme énoncé précédement, la situation sanitaire à beaucoup impacter la gestion de notre projet. Des outils tels que [Trello](https://trello.com/guide?utm_source=trello&utm_medium=inapp&utm_content=header-tips&utm_campaign=guide) et son PowerUp [TeamGantt](https://trello.com/power-ups/5970d4298c14fdf691c95a76/teamgantt) ont aidé à maintenir le cap tout au long de son élaboration.
